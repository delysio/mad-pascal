program test;

uses 
    atari, crt;

const
    BUILD_DATE = {$INCLUDE %DATE%};

    display_list_adr    = $0600;
    scroll_font         = $4000;
    scroll_font_ready   = $4500;
    screen_adr_1        = $5000;
    screen_adr_2        = $6000;

    scroll_text         = '                     WELCOME TO THE AWESOME SINUS SCROLL WITH TWO BY TWO FONT      IT WAS WRITTEN ENTIRELY IN MAD PASCAL     GREETINGS TO ALL MADPASCAL CODERS     ESPECIALLY FOR GUYS OF MADPASCALERS GROUP                     '#0;

    mul48: array[0..32] of word = (0,48,96,144,192,240,288,336,384,432,480,528,576,624,672,720,768,816,864,912,960,1008,1056,1104,1152,1200,1248,1296,1344,1392,1440,1488,1536);
    mul54: array[0..32] of word = (0,54,108,162,216,270,324,378,432,486,540,594,648,702,756,810,864,918,972,1026,1080,1134,1188,1242,1296,1350,1404,1458,1512,1566,1620,1674,1728);

    sin_table: array[0..39] of byte = (6,5,4,3,2,1,1,0,0,0,0,0,0,0,1,2,     2,3,4,5,6,7,8,9,9,10,11,11,11,11,11,11,     11,10,10,9,8,7,6,6);

var 
    display_list: array [0..55] of byte = (
        $70, $70, $70, $70, $70, $70, $70, $70, $70, $70,
        $70, $70, $70, $70, $70, $70, $70, $70, $70, 
        $5e, $00, $60, $1e, $1e, $1e, $1e, $1e, $1e, 
        $1e, $1e, $1e, $1e, $1e, $1e, $1e, $1e, $1e,
        $1e, $1e, $1e, $1e, $1e, $1e, $1e, $1e, 
        $1e, $1e, $1e, $1e, $1e, $1e, $1e, $1e, 
        $41, $00, $06
    );

var
    i, j, k, m, o, sin_value: byte;
    n, mul_poke, mul_peek: word;
    current_screen: byte = 0;
    letter_counter: word = 0;
    letter_counter_backup: word = 0;

{$R sinusScroll.rc}

procedure drawLetters(o: byte);
(*
* @description:
* Prints letter on hidden screen
*
* @param: (byte) o - number of column to print
*)
var 
    letter_position: byte;
    letter_offset: byte;
    letter: byte;

begin

    // Get code of letter

    letter := Ord(scroll_text[letter_counter]);

    // Calculate position in font definition pic ATASCII/INTERNAL code table is handy
    // https://www.atariarchives.org/mapping/appendix10.php

    letter_position := letter - 65;

    // Not letter? Print space

    if letter_position > 25 then letter_position := 26;

    // Font is 2 bytes wide, so multiply x2

    letter_offset := letter_position shl 1;

    // Do twice - font is 2 bytes wide

    for i := 0 to 1 do begin

        // Get sinus value - m holds index to sinus table

        sin_value := sin_table[m];

        // Get address of column start in memory, we use precalculated multiplication

        n := mul48[sin_value];

        // Write 16 bytes of font

        for j := 15 downto 0 do begin

            // Add sinus value to address in memory

            mul_poke := mul48[j] + n;

            // Get address of byte in font definition

            mul_peek := mul54[j];

            // k holds value to write, i - number of column of char

            k := Peek(scroll_font_ready + i + mul_peek + letter_offset);

            // o - column of scroll

            if current_screen = 2 then begin Poke(screen_adr_1 + mul_poke + o, k) end else Poke(screen_adr_2 + mul_poke + o, k);

        end;

        // Increase index of sinus table

        Inc(m);

        // Check if it is end of sinus table, if so - wrap it

        if m > 39 then m := 0;

        // Increase column index

        Inc(o);

    end;

    // Get next letter

    letter_counter := letter_counter + 1;

end;


procedure prepare_font_def;
var 
    i: byte;
    j: byte;
    k: word;
    l: word;

begin

    k := 0;     // destination line index 
    l := 0;     // source line index

    // first line of mic picture
    // data is organized this way:
    // ABCDEFGHIJKLMNOPQRST
    // UVWXYZ
    //
    // So we need to organise it in linear way (for calculation simplicity)

    // Copy first line

    for j := 0 to 15 do begin

        for i := 0 to 39 do Poke(scroll_font_ready + i + k, Peek(scroll_font + i + l));

        k := k + 54;    // 54 = 2 * 26 (letters) + 2 (space)
        l := l + 40;    // 40 bytes in source pic line

    end;

    k := 0;
    l := 0;

    // Copy second line

    for j := 0 to 15 do begin
            
        for i := 0 to 11 do Poke(scroll_font_ready + 40 + i + k, Peek(scroll_font + 16 * 40 + i + l)); // 16 * 40 because second line is 2 bytes lower in pic

        k := k + 54;
        l := l + 40;

    end;

end;



begin

    CH := $ff;

    // Clear screen area

    fillbyte(pointer(screen_adr_1), 48*32, 0);
    fillbyte(pointer(screen_adr_2), 48*32, 0);

    // Prepare font definition - set up linearly

    prepare_font_def;

    // Set display list 

    Pause;
    move(@display_list, pointer(display_list_adr), sizeOf(display_list));
    SDLSTL := $0600;
    Pause;

    // Set sinus table index to beginning of the table

    m := 0; 

    // Set screen to first buffer

    current_screen := 1;

    // Set index to scroll content

    letter_counter := 0;
    letter_counter_backup := 0;

    // Set colors

    COLOR0 := $D6;
    COLOR1 := $4A;
    COLOR2 := $1C;

    // Main loop

    repeat

        // Increase index of scroll letters

        Inc(letter_counter_backup);

        // Set up wrapping

        if letter_counter_backup > 205 then letter_counter_backup := 1;

        // Remember index

        letter_counter := letter_counter_backup;

        // Page flipping
        // Display ready to show screen, and write new letter and sinus to another

        // In first frame we need to switch display list to prepared screen

        Pause;
        HSCROL := 7;
        if current_screen = 1 then begin

            // Set display list's LMS to ready screen and clear screen

            Dpoke(display_list_adr+20, screen_adr_1);

            // Clear screen

            Fillbyte(pointer(screen_adr_2), 32*54, 0);

        end else begin

            Dpoke(display_list_adr+20, screen_adr_2);
            Fillbyte(pointer(screen_adr_1), 32*54, 0);
            
        end;
        
        drawLetters(1);   
        drawLetters(3);  

        // Second frame... 

        Pause;             
        HSCROL := 6;
        drawLetters(5);   
        drawLetters(7);   
        drawLetters(9); 

        Pause;              
        HSCROL := 5;
        drawLetters(11);  
        drawLetters(13);  
        drawLetters(15);   

        Pause;              
        HSCROL := 4;
        drawLetters(17);  
        drawLetters(19);   
        drawLetters(21);   

        Pause; 
        HSCROL := 3;
        drawLetters(23); 
        drawLetters(25);   
        drawLetters(27);   

        Pause;              
        HSCROL := 2;
        drawLetters(29);   
        drawLetters(31);   
        drawLetters(33);

        Pause;              
        HSCROL := 1;
        drawLetters(35);
        drawLetters(37);
        drawLetters(39);

        // Flip screen

        Pause;   
        drawLetters(41);
        drawLetters(43);          
        HSCROL := 0;
        if current_screen = 1 then current_screen := 2 else current_screen := 1;

    until keypressed;

    CH := $ff;
    pause;

end.

