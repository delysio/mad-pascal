program three_d;

uses 
    atari, crt, fastgraph;

const
    BUILD_DATE = {$INCLUDE %DATE%};

var  
    sz                  : byte = 16;
    cx                  : byte = 79;
    cy                  : byte = 50;
    sc                  : byte = 0;
    angle               : float16 = 0.0;

    pts                 : array[0..7, 0..2] of shortint;
    rzp                 : array[0..7, 0..2] of float16;
    ryp                 : array[0..7, 0..2] of float16;
    rxp                 : array[0..7, 0..2] of float16;

    X1, X2, Y1, Y2      : byte;
    A, B                : byte;

    var	buf1, buf2      : TDisplayBuffer;

    rtc: byte absolute 20;

procedure init_points;
begin
    pts[0,0] := -1; pts[0,1] := -1; pts[0,2] := -1;
    pts[1,0] :=  1; pts[1,1] := -1; pts[1,2] := -1;
    pts[2,0] :=  1; pts[2,1] :=  1; pts[2,2] := -1;
    pts[3,0] := -1; pts[3,1] :=  1; pts[3,2] := -1;
    pts[4,0] := -1; pts[4,1] := -1; pts[4,2] :=  1;
    pts[5,0] :=  1; pts[5,1] := -1; pts[5,2] :=  1;
    pts[6,0] :=  1; pts[6,1] :=  1; pts[6,2] :=  1;
    pts[7,0] := -1; pts[7,1] :=  1; pts[7,2] :=  1;
end;

procedure rotate_points;
var 
    n: byte;
    sn, cs: float16;
begin
    sn := sin(angle);
    cs := cos(angle);

        rzp[0,0] := -cs * pts[0,0] + sn * pts[0,1];
        rzp[0,1] :=  sn * pts[0,0] + cs * pts[0,1];
        rzp[0,2] :=  pts[0,2];
        ryp[0,0] :=  cs * rzp[0,0] +  sn * rzp[0,2];
        ryp[0,1] :=  rzp[0,1];
        ryp[0,2] := -sn * rzp[0,0] +  cs * rzp[0,2];
        rxp[0,0] :=  ryp[0,0];
        rxp[0,1] :=  cs * ryp[0,1] - sn * ryp[0,2];
        rxp[0,2] :=  sn * ryp[0,1] + cs * ryp[0,2];

        rzp[1,0] := -cs * pts[1,0] + sn * pts[1,1];
        rzp[1,1] :=  sn * pts[1,0] + cs * pts[1,1];
        rzp[1,2] :=  pts[1,2];
        ryp[1,0] :=  cs * rzp[1,0] +  sn * rzp[1,2];
        ryp[1,1] :=  rzp[1,1];
        ryp[1,2] := -sn * rzp[1,0] +  cs * rzp[1,2];
        rxp[1,0] :=  ryp[1,0];
        rxp[1,1] :=  cs * ryp[1,1] - sn * ryp[1,2];
        rxp[1,2] :=  sn * ryp[1,1] + cs * ryp[1,2];

        rzp[2,0] := -cs * pts[2,0] + sn * pts[2,1];
        rzp[2,1] :=  sn * pts[2,0] + cs * pts[2,1];
        rzp[2,2] :=  pts[2,2];
        ryp[2,0] :=  cs * rzp[2,0] +  sn * rzp[2,2];
        ryp[2,1] :=  rzp[2,1];
        ryp[2,2] := -sn * rzp[2,0] +  cs * rzp[2,2];
        rxp[2,0] :=  ryp[2,0];
        rxp[2,1] :=  cs * ryp[2,1] - sn * ryp[2,2];
        rxp[2,2] :=  sn * ryp[2,1] + cs * ryp[2,2];

        rzp[3,0] := -cs * pts[3,0] + sn * pts[3,1];
        rzp[3,1] :=  sn * pts[3,0] + cs * pts[3,1];
        rzp[3,2] :=  pts[3,2];
        ryp[3,0] :=  cs * rzp[3,0] +  sn * rzp[3,2];
        ryp[3,1] :=  rzp[3,1];
        ryp[3,2] := -sn * rzp[3,0] +  cs * rzp[3,2];
        rxp[3,0] :=  ryp[3,0];
        rxp[3,1] :=  cs * ryp[3,1] - sn * ryp[3,2];
        rxp[3,2] :=  sn * ryp[3,1] + cs * ryp[3,2];

        rzp[4,0] := -cs * pts[4,0] + sn * pts[4,1];
        rzp[4,1] :=  sn * pts[4,0] + cs * pts[4,1];
        rzp[4,2] :=  pts[4,2];
        ryp[4,0] :=  cs * rzp[4,0] +  sn * rzp[4,2];
        ryp[4,1] :=  rzp[4,1];
        ryp[4,2] := -sn * rzp[4,0] +  cs * rzp[4,2];
        rxp[4,0] :=  ryp[4,0];
        rxp[4,1] :=  cs * ryp[4,1] - sn * ryp[4,2];
        rxp[4,2] :=  sn * ryp[4,1] + cs * ryp[4,2];

        rzp[5,0] := -cs * pts[5,0] + sn * pts[5,1];
        rzp[5,1] :=  sn * pts[5,0] + cs * pts[5,1];
        rzp[5,2] :=  pts[5,2];
        ryp[5,0] :=  cs * rzp[5,0] +  sn * rzp[5,2];
        ryp[5,1] :=  rzp[5,1];
        ryp[5,2] := -sn * rzp[5,0] +  cs * rzp[5,2];
        rxp[5,0] :=  ryp[5,0];
        rxp[5,1] :=  cs * ryp[5,1] - sn * ryp[5,2];
        rxp[5,2] :=  sn * ryp[5,1] + cs * ryp[5,2];

        rzp[6,0] := -cs * pts[6,0] + sn * pts[6,1];
        rzp[6,1] :=  sn * pts[6,0] + cs * pts[6,1];
        rzp[6,2] :=  pts[6,2];
        ryp[6,0] :=  cs * rzp[6,0] +  sn * rzp[6,2];
        ryp[6,1] :=  rzp[6,1];
        ryp[6,2] := -sn * rzp[6,0] +  cs * rzp[6,2];
        rxp[6,0] :=  ryp[6,0];
        rxp[6,1] :=  cs * ryp[6,1] - sn * ryp[6,2];
        rxp[6,2] :=  sn * ryp[6,1] + cs * ryp[6,2];

        rzp[7,0] := -cs * pts[7,0] + sn * pts[7,1];
        rzp[7,1] :=  sn * pts[7,0] + cs * pts[7,1];
        rzp[7,2] :=  pts[7,2];
        ryp[7,0] :=  cs * rzp[7,0] +  sn * rzp[7,2];
        ryp[7,1] :=  rzp[7,1];
        ryp[7,2] := -sn * rzp[7,0] +  cs * rzp[7,2];
        rxp[7,0] :=  ryp[7,0];
        rxp[7,1] :=  cs * ryp[7,1] - sn * ryp[7,2];
        rxp[7,2] :=  sn * ryp[7,1] + cs * ryp[7,2];
    
end;

procedure draw_lines;
begin
    X1 := Trunc(ryp[0,0] * sz + cx);
    Y1 := Trunc(ryp[0,1] * sz + cy);
    X2 := Trunc(ryp[1,0] * sz + cx);
    Y2 := Trunc(ryp[1,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);

    X1 := Trunc(ryp[1,0] * sz + cx);
    Y1 := Trunc(ryp[1,1] * sz + cy);
    X2 := Trunc(ryp[2,0] * sz + cx);
    Y2 := Trunc(ryp[2,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);

    X1 := Trunc(ryp[2,0] * sz + cx);
    Y1 := Trunc(ryp[2,1] * sz + cy);
    X2 := Trunc(ryp[3,0] * sz + cx);
    Y2 := Trunc(ryp[3,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);

    X1 := Trunc(ryp[3,0] * sz + cx);
    Y1 := Trunc(ryp[3,1] * sz + cy);
    X2 := Trunc(ryp[0,0] * sz + cx);
    Y2 := Trunc(ryp[0,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);

    X1 := Trunc(ryp[4,0] * sz + cx);
    Y1 := Trunc(ryp[4,1] * sz + cy);
    X2 := Trunc(ryp[5,0] * sz + cx);
    Y2 := Trunc(ryp[5,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);

    X1 := Trunc(ryp[5,0] * sz + cx);
    Y1 := Trunc(ryp[5,1] * sz + cy);
    X2 := Trunc(ryp[6,0] * sz + cx);
    Y2 := Trunc(ryp[6,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);

    X1 := Trunc(ryp[6,0] * sz + cx);
    Y1 := Trunc(ryp[6,1] * sz + cy);
    X2 := Trunc(ryp[7,0] * sz + cx);
    Y2 := Trunc(ryp[7,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);

    X1 := Trunc(ryp[7,0] * sz + cx);
    Y1 := Trunc(ryp[7,1] * sz + cy);
    X2 := Trunc(ryp[4,0] * sz + cx);
    Y2 := Trunc(ryp[4,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);

    X1 := Trunc(ryp[0,0] * sz + cx);
    Y1 := Trunc(ryp[0,1] * sz + cy);
    X2 := Trunc(ryp[4,0] * sz + cx);
    Y2 := Trunc(ryp[4,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);

    X1 := Trunc(ryp[1,0] * sz + cx);
    Y1 := Trunc(ryp[1,1] * sz + cy);
    X2 := Trunc(ryp[5,0] * sz + cx);
    Y2 := Trunc(ryp[5,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);

    X1 := Trunc(ryp[2,0] * sz + cx);
    Y1 := Trunc(ryp[2,1] * sz + cy);
    X2 := Trunc(ryp[6,0] * sz + cx);
    Y2 := Trunc(ryp[6,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);

    X1 := Trunc(ryp[3,0] * sz + cx);
    Y1 := Trunc(ryp[3,1] * sz + cy);
    X2 := Trunc(ryp[7,0] * sz + cx);
    Y2 := Trunc(ryp[7,1] * sz + cy);

    fLine(X1,Y1,X2,Y2);
    
end;

begin
    NewDisplayBuffer(buf1, 7 + 16, $c0);		// ramtop = $c0
    NewDisplayBuffer(buf2, 7 + 16, $a0);		// ramtop = $a0

    SetColor(1);

    init_points;

    repeat
        pause;
        SwitchDisplayBuffer(buf1, buf2);
        rotate_points;
        draw_lines;
        angle := angle + 0.1;
    until keypressed;

end.
