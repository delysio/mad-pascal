procedure multiMove (src: pointer;  dest1: pointer; dest2: pointer; size: byte); assembler; overload;
(*
* @description:
* Copies up to a page of memory to more than one location. 
* Due to the desired high speed, 6 cells are used on the zero page 
* This routine copies to two locations.
*
* @param: (pointer) src - source address.
* @param: (pointer) destination 1 - first destination address
* @param: (pointer) destination 2 - second destination address
* @param: (byte) size - size of data to be copied
*) 
asm {

ZP  = $f0

    dec size

    lda src
    sta ZP
    lda src+1
    sta ZP+1

    lda dest1
    sta ZP+2
    lda dest1+1
    sta ZP+3

    lda dest2
    sta ZP+4
    lda dest2+1
    sta ZP+5

    ldy size
@   lda (ZP),y
    sta (ZP+2),y
    sta (ZP+4),y
    dey
    bpl @-

}; 
end;

(*
* @description:
* Copies up to a page of memory to more than one location.
* Due to the desired high speed, 8 cells are used on the zero page 
* This routine copies to three locations.
*
* @param: (pointer) src - source address.
* @param: (pointer) destination 1 - first destination address
* @param: (pointer) destination 2 - second destination address
* @param: (pointer) destination 3 - third destination address
* @param: (byte) size - size of data to be copied
*) 
procedure multiMove (src: pointer;  dest1: pointer; dest2: pointer; dest3: pointer; size: byte); assembler; overload;
asm {

ZP  = $f0

    dec size

    lda src
    sta ZP
    lda src+1
    sta ZP+1

    lda dest1
    sta ZP+2
    lda dest1+1
    sta ZP+3

    lda dest2
    sta ZP+4
    lda dest2+1
    sta ZP+5

    lda dest3
    sta ZP+6
    lda dest3+1
    sta ZP+7

    ldy size
@   lda (ZP),y
    sta (ZP+2),y
    sta (ZP+3),y
    sta (ZP+4),y
    dey
    bpl @-

}; 
end;
